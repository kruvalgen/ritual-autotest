package tests;

import com.microsoft.playwright.Page;
import org.testng.annotations.Test;
import base.BaseTest;
import ru.at.ritual.pages.LoginPage;
import ru.at.ritual.pages.MainPage;
import ru.at.ritual.utils.Sleep;
import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class ТМ3ID1 extends BaseTest {

    @Test(description = "Проверка формы оперативной статистики для оценки объема выполненных работ по созданию разбивочных чертежей (инвентаризации) на кладбище")
    public void ID1(){
        Page page = context.newPage();
        MainPage mainPage = new MainPage(page);
        LoginPage loginPage = new LoginPage(page);
        loginPage.navigate();
        loginPage.login("Григорьева А.А.");
        loginPage.loginButton.click();
        Sleep.pauseMs(1000);
        mainPage.button("Проверка кладбищ").click();
        mainPage.dropdownMenu("Проверка кладбищ").click();
        mainPage.searchInput().click();
        mainPage.listItem("Былово-1").click();
        assertThat(mainPage.elementsInTable("Былово-1"))
                .containsText(new String[] {"Всего (геодезия):", "Сформировано участков:", "Внесены фотографии:",
                        "Всего создано описаний:", "Не поступило на проверку 1:", "Из них проверка 1:", "Проверка 2:",
                        "Пройден 2й уровень:", "Нмс на иностранном языке:"
                });
    }




}
