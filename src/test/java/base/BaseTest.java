package base;

import com.microsoft.playwright.*;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import ru.at.ritual.utils.properties.Configurations;

public class BaseTest {

    static protected Browser browser;
    protected BrowserContext context;

    @BeforeClass
    public void setUp(){
        browser = Playwright
                .create()
                .chromium()
                .launch(new BrowserType.LaunchOptions()
                        .setHeadless(false).setChannel("chrome"));

    }

    @BeforeMethod
    public void setContext(){
        context = browser.newContext();
    }

    @AfterMethod
    public void closeSession(){
        context.close();
    }

    @AfterClass
    public void tearDown(){
        browser.close();
    }

}
