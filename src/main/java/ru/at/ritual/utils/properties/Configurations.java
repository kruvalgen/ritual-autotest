package ru.at.ritual.utils.properties;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "classpath:config/credentials.properties",
        "system:properties"
})
public interface Configurations extends Config {

    @Key("baseurl")
    @DefaultValue("")
    String getBaseURL();
}
