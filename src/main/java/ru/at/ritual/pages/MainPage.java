package ru.at.ritual.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

public class MainPage {
    private final Page page;

    public MainPage(Page page) {
        this.page = page;
    }
    public Locator button(String elementName){
        return page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName(elementName));
    }
    public Locator dropdownMenu(String elementName){
        return page.locator(String.format("//ul[@class='dropdown-menu']//a[text()='%s']", elementName));
    }
    public Locator searchInput(){
        return page.locator("//div[@class='select-search']//input");
    }
    public Locator listItem(String elementName){
        return page.getByRole(AriaRole.LISTITEM).filter(new Locator.FilterOptions().setHasText(elementName));
    }
    public Locator elementsInTable(String elementName){
        return page.locator(String.format("//div[./h2[text()='%s']]//tr", elementName));
    }



}
