package ru.at.ritual.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.aeonbits.owner.ConfigFactory;
import ru.at.ritual.helpers.GetLoginData;
import ru.at.ritual.helpers.User;
import ru.at.ritual.utils.properties.Configurations;

public class LoginPage {
    Configurations conf = ConfigFactory.create(Configurations.class, System.getProperties());
    private final Page page;
    public final Locator loginInput;
    public final Locator passwordInput;
    public final Locator loginButton;

    public LoginPage(Page page) {
        this.page = page;
        this.loginInput = page.locator("//input[@name='Login']");
        this.passwordInput = page.locator("//input[@name='Password']");
        this.loginButton = page.locator("//input[@value='Войти']");
    }

    public void login(String user){
        loginInput.fill(user);
        passwordInput.fill(GetLoginData.getPassword(user));
    }

    public void navigate(){
        page.navigate(conf.getBaseURL());
    }

}
