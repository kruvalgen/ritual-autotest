package ru.at.ritual.helpers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class GetLoginData {

    private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    public static String getPassword(String login){
        return LoginList.getLoginList().getList().get(login);
    }

    public static class LoginList{
        private static LoginList loginList;
        public static LoginList getLoginList(){
            if (loginList == null){
                try {
                    loginList = mapper.readValue(new File("src/test/resources/config/loginList.yml"), LoginList.class);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return loginList;
            }

        @JsonProperty("loginList")
        private Map<String, String> list;

        public Map<String, String> getList(){
            return list;
            }
        }
    }

